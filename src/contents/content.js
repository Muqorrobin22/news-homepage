export const headerContent = {
  home: "Home",
  new: "New",
  popular: "Popular",
  categories: "Categories",
  trending: "Trending",
};

export const mainContent = [
  {
    id: 1,
    title: "Hydrogen VS Electric Cars",
    desc: "Will hydrogen-fueled cars ever catch up to EVs?",
  },
  {
    id: 2,
    title: "The Downsides of AI Artistry",
    desc: "What are the possible adverse effects of on-demand AI image generation?",
  },
  {
    id: 3,
    title: "Is VC Funding Drying Up?",
    desc: "Private funding by VC firms is down 50% YOY. We take a look at what that means.",
  },
];

export const mainContentBottom = {
  heroTitle: "The Bright Future of Web 3.0?",
  heroDesc:
    "We dive into the next evolution of the web that claims to put the power of the platforms back into the hands of the people. But is it really fulfilling its promise?",
  readMore: "Read More",
};

export const images = {
  hero: "/images/image-web-3-desktop.jpg",
  heroMobile: "/images/image-web-3-mobile.jpg",
};

export const footerContent = [
  {
    id: 1,
    image: "/images/image-retro-pcs.jpg",
    no: "01",
    title: "Reviving Retro PCs",
    desc: "What happens when old PCs are given modern upgrades?",
  },
  {
    id: 2,
    image: "/images/image-top-laptops.jpg",
    no: "02",
    title: "Top 10 Laptops of 2022",
    desc: "Our best picks for various needs and budgets.",
  },
  {
    id: 3,
    image: "/images/image-gaming-growth.jpg",
    no: "03",
    title: "The Growth of Gaming",
    desc: "How the pandemic has sparked fresh opportunities.",
  },
];
