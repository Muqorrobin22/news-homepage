import "./App.css";
import { Aside } from "./components/Card/Aside";
import { FooterCard } from "./components/Card/FooterCard";
import { Header } from "./components/Header/Header";
import {
  footerContent,
  images,
  mainContent,
  mainContentBottom,
} from "./contents/content";

function App() {
  const { hero, heroMobile } = images;

  return (
    <main className="app">
      {/* Header */}
      <Header />

      {/* Main */}
      <figure
        className="app__hero"
        style={{ backgroundImage: `url(${hero})` }}
      ></figure>
      <div className="app__aside">
        <h1 className="app__aside--heading">New</h1>
        {mainContent.map((content, index) => (
          <Aside key={content.id} content={content} index={index} />
        ))}
      </div>
      <div className="app__bottom">
        <h1 className="app__bottom--heading">{mainContentBottom.heroTitle}</h1>

        <div className="app__bottom--wrap">
          <p className="app__bottom--wrap-paragraph">
            {mainContentBottom.heroDesc}
          </p>
          <a
            href="#"
            aria-label="Read More"
            className="app__bottom--wrap-button"
          >
            {mainContentBottom.readMore}
          </a>
        </div>
      </div>

      {/* Footer */}
      {footerContent.map((content) => (
        <FooterCard key={content.id} content={content} />
      ))}
    </main>
  );
}

export default App;
