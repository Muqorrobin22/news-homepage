import "./FooterCard.css";

export function FooterCard({ content }) {
  return (
    <div className="footer__card">
      <figure
        className="card__images"
        style={{ backgroundImage: `url(${content.image})` }}
      />

      <div className="card__info">
        <h1 className="card__info--heading-1">{content.no}</h1>
        <h2 className="card__info--heading-2">{content.title}</h2>
        <p className="card__info--paragraph">{content.desc}</p>
      </div>
    </div>
  );
}
