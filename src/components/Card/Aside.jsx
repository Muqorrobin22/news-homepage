import "./Aside.css";

export function Aside({ content, index }) {
  return (
    <aside className="app__aside--card">
      <h2 className="app__aside--heading-2">{content.title}</h2>
      <p className="app__aside--paragraph">{content.desc}</p>

      {index === 2 ? "" : <div className="app__aside--line" />}
    </aside>
  );
}
