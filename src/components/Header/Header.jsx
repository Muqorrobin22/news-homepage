import { useState } from "react";
import { headerContent } from "../../contents/content";
import "./Header.css";

export function Header() {
  const [isMobile, setIsMobile] = useState(false);
  const [isOpen, setIsOpen] = useState(false);

  const handleMenu = function toggleHamburger() {
    setIsOpen((current) => !current);
  };

  const handleIcon = function toggleIconMenu() {
    setIsMobile((current) => !current);
  };

  return (
    <header className="header">
      <img src="/images/logo.svg" alt="Gambar Logo" className="logo__image" />

      <img
        src={isOpen ? "/images/icon-menu-close.svg" : "/images/icon-menu.svg"}
        alt="hamburger Menu"
        className={isMobile ? "" : "isMobile"}
        onClick={handleMenu}
      />

      <nav className={`nav ${isOpen ? "isOpen" : ""}`}>
        <ul>
          <li>
            <a href="#">{headerContent.home}</a>
          </li>
          <li>
            <a href="#">{headerContent.new}</a>
          </li>
          <li>
            <a href="#">{headerContent.popular}</a>
          </li>
          <li>
            <a href="#">{headerContent.trending}</a>
          </li>
          <li>
            <a href="#">{headerContent.categories}</a>
          </li>
        </ul>
      </nav>

      <div className={`backdrop ${isOpen ? "isOpen" : ""}`}></div>
    </header>
  );
}
